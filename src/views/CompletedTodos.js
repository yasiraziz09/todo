import React from 'react';
import { View, FlatList, Text, Image, Alert,TouchableOpacity } from 'react-native';
import {  useDispatch, useSelector } from 'react-redux';
import { Header } from '../components';
import {Constants,Colors} from '../common';
import TodoItem from '../components/todoItem';
import Swipeable from 'react-native-swipeable';

const CompletedTodos = (props) => {
    const todosCompleted = useSelector(state => state.main.todosCompleted);
    const dispatch = useDispatch()
    const onDelete = (index) => {
        Alert.alert(
            'Confirm!',
            'Are you sure you want to delete?',
            [
              { text: 'NO', onPress: () => {} },
              { text: 'YES', onPress: () => {
                let _todosCompleted = [...todosCompleted];
                _todosCompleted.splice(index,1);
                dispatch({type:'UPDATE_TODOS_COMPLETED',payload:_todosCompleted});
              } },
            ],
            { cancelable: false }
        )
    }

    const renderTodo = ({item,index})=> (
        <Swipeable
            rightButtons={[
                <TouchableOpacity onPress={() => onDelete(index)} style={[styles.rightSwipeItem, {backgroundColor: '#c0392b'}]}>
                <Text style={{color:Colors.white,fontSize:12}}>Delete</Text>
                </TouchableOpacity>
            ]}
            >
            <TodoItem item={item} showDoneButton={false} />
        </Swipeable>
    )

    return(
        <View style={{flex:1}}>
            <Header titleText='TODO' />
            <View style={{flex:1}}>
                {todosCompleted.length > 0 ? <FlatList
                    data={todosCompleted}
                    keyExtractor={(item,index) => index.toString()}
                    renderItem={renderTodo}
                /> : <View style={styles.placeholderView}>
                <Image style={styles.placeholderImage} source={Constants.check} />
                <Text style={styles.placeholderText}>
                    No Task in To-Do
                </Text>
            </View>}
            </View>
        </View>
    );
}

const styles = {
    rightSwipeItem: {
        marginBottom:5,
        height:70,
        width:75,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkIcon:{
        width:25,
        height:25,
        marginRight:15,
        resizeMode:'contain',
        tintColor:Colors.borderColor
    },
    newTodo:{
        padding:12,
        paddingVertical:20,
        borderBottomColor:Colors.borderColor,
        borderTopColor:Colors.borderColor,
        borderBottomWidth:0.5,
        borderTopWidth:0.5
    },
    newTodoContainer:{
        flexDirection:'row',
        alignItems: 'center',
        marginBottom:10
    },
    inputContainer:{
        flex:1, 
        paddingRight:12,
        borderRadius:5,
        borderWidth:.5,
        padding:7,
        borderColor:Colors.borderColor
    },
    addButton:{
        backgroundColor:Colors.primaryBlue,
        marginTop:10
    },
    addButtonText:{
        textAlign:'center',
        paddingVertical:10,
        color:Colors.white,
    },
    todoItem:{
        flexDirection:'row',
        marginVertical:5,
        paddingHorizontal:12,
        height:70,
        alignItems: 'center',
        borderBottomWidth:0.5,
        borderBottomColor:Colors.borderColor
    },
    todoItemTitle:{
        flex:1,
        fontSize:18,
        fontWeight:'600',
        paddingRight:20
    },
    dateText:{
        color:Colors.red
    },
    dateButton:{
        flexDirection:'row',
        alignItems: 'center'
    },
    calendarImage:{
        width:30, 
        height:30,
        resizeMode:'contain',
        marginHorizontal:5,
        tintColor:Colors.red

    },
    placeholderView:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    placeholderImage:{
        width:Constants.width/2,
        height:Constants.width/2,
        resizeMode:'contain',
        tintColor:'#bdc3c7'
    },
    placeholderText:{
        textAlign:'center',
        fontSize:20,
        color:'#bdc3c7'
    }
}

export default CompletedTodos;