import React, {useEffect } from 'react';
import {
    Image,
    View,
} from 'react-native';
import {Constants,Colors} from '../common';

const Splash = ({
    navigation,
}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('main');
        }, 2500);
    },[]);

    return(
    <View style={{flex:1,backgroundColor:Colors.primaryBlue, justifyContent:'center', alignItems: 'center',}}>
        <Image style={{ width: "50%", height: "50%", resizeMode: "contain" }} source={Constants.check} />
    </View>
    );
}
export default Splash;