import React, { useRef, useState } from 'react';
import { View, TextInput, Pressable, FlatList, Text, Image, Alert, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Header } from '../components';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import {Constants,Colors} from '../common';
import TodoItem from '../components/todoItem';
import Swipeable from 'react-native-swipeable';


const initialState = {
    isDatePickerVisible:false,
    title:'',
    date:'',
}

const Main = (props) => {
    const [
        {isDatePickerVisible,title,date},
        setState
    ] = useState(initialState)
    const dispatch = useDispatch();
    const todos = useSelector(state => state.main.todos);
    const todosCompleted = useSelector(state => state.main.todosCompleted);
    const showDatePicker = () => {
        updateState({isDatePickerVisible:true})
    };
    
    const hideDatePicker = () => {
        updateState({isDatePickerVisible:false})
    };

    const updateState = (newState) => {
        setState(prevState => ({...prevState , ...newState}))
    }
    
    const handleConfirm = (date) => {
        var _date = moment(date).format('DD MMM');
        hideDatePicker();
        updateState({date:_date});
    };

    onSubmit = () => {
        input.current.blur()
        if(date == ''){
            showDatePicker();
        }else{
            let _todos = [...todos];
            var todo = {
                title,
                date
            }
            _todos.push(todo);
            dispatch({
                type:'UPDATE_TODOS',
                payload:_todos
            });
            updateState(initialState)
        }
    }

    const markDone = (item,index) => {
        Alert.alert(
            'Confirm!',
            'Are you sure you want to mark done?',
            [
              { text: 'NO', onPress: () => {} },
              { text: 'YES', onPress: () => {
                let _todos = [...todos];
                let _todosCompleted = [...todosCompleted];
                _todosCompleted.push(item)
                dispatch({type:'UPDATE_TODOS_COMPLETED',payload:_todosCompleted});
                _todos.splice(index,1);
                dispatch({type:'UPDATE_TODOS',payload:_todos});
              } },
            ],
            { cancelable: false }
        )
    }

    const onDelete = (index) => {
        Alert.alert(
            'Confirm!',
            'Are you sure you want to delete?',
            [
              { text: 'NO', onPress: () => {} },
              { text: 'YES', onPress: () => {
                let _todos = [...todos];
                _todos.splice(index,1);
                dispatch({type:'UPDATE_TODOS',payload:_todos});
              } },
            ],
            { cancelable: false }
        )
    }
    
    
    const renderTodoItem = ({item,index})=> {
        return <Swipeable
            rightButtons={[
                <TouchableOpacity onPress={() => onDelete(index)} style={[styles.rightSwipeItem, {backgroundColor: '#c0392b'}]}>
                <Text style={{color:Colors.white,fontSize:12}}>Delete</Text>
                </TouchableOpacity>
            ]}
            >
        <TodoItem item={item} index={index} onPress={markDone}  />
        </Swipeable>
    }

    const input = useRef();
    
    return(
        <View style={{flex:1}}>
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
                minimumDate={new Date()}
            />
            <Header titleText='TODO' />
            <View style={styles.newTodo}>
                <View style={styles.newTodoContainer}>
                    <View style={styles.inputContainer}>
                        <TextInput  
                            ref={input}
                            returnKeyType='done'
                            autoFocus
                            placeholder={'Title'}
                            onChangeText={(value) => updateState({title:value})}
                            value={title}
                        />
                    </View>
                    <Pressable style={styles.dateButton} onPress={() => showDatePicker()}>
                        <Image style={styles.calendarImage} source={Constants.calendar} />
                        <Text style={styles.dateText}>{date}</Text>
                    </Pressable>
                </View>
                <Pressable 
                disabled={title == '' ? true : false}
                style={[styles.addButton,title == '' ? {opacity:0.6} :null]}
                onPress={() => onSubmit()}>
                    <Text style={styles.addButtonText}>Add</Text>
                </Pressable>
            </View>
            <View style={{flex:1}}>
                {todos.length > 0 ? <FlatList
                    data={todos}
                    keyExtractor={(item,index) => index.toString()}
                    renderItem={renderTodoItem}
                /> : <View style={styles.placeholderView}>
                <Image style={styles.placeholderImage} source={Constants.check} />
                <Text style={styles.placeholderText}>
                    No Task in To-Do
                </Text>
            </View>}
            </View>
        </View>
    );
}

const styles = {
    rightSwipeItem: {
        marginBottom:5,
        height:70,
        width:75,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkIcon:{
        width:25,
        height:25,
        marginRight:15,
        resizeMode:'contain',
        tintColor:Colors.borderColor
    },
    newTodo:{
        padding:12,
        paddingVertical:20,
        borderBottomColor:Colors.borderColor,
        borderTopColor:Colors.borderColor,
        borderBottomWidth:0.5,
        borderTopWidth:0.5
    },
    newTodoContainer:{
        flexDirection:'row',
        alignItems: 'center',
        marginBottom:10
    },
    inputContainer:{
        flex:1, 
        paddingRight:12,
        borderRadius:5,
        borderWidth:.5,
        padding:7,
        borderColor:Colors.borderColor
    },
    addButton:{
        backgroundColor:Colors.primaryBlue,
        marginTop:10
    },
    addButtonText:{
        textAlign:'center',
        paddingVertical:10,
        color:Colors.white,
    },
    todoItem:{
        flexDirection:'row',
        marginVertical:5,
        paddingHorizontal:12,
        height:70,
        alignItems: 'center',
        borderBottomWidth:0.5,
        borderBottomColor:Colors.borderColor
    },
    todoItemTitle:{
        flex:1,
        fontSize:18,
        fontWeight:'600',
        paddingRight:20
    },
    dateText:{
        color:Colors.red
    },
    dateButton:{
        flexDirection:'row',
        alignItems: 'center'
    },
    calendarImage:{
        width:30, 
        height:30,
        resizeMode:'contain',
        marginHorizontal:5,
        tintColor:Colors.red

    },
    placeholderView:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    placeholderImage:{
        width:Constants.width/2,
        height:Constants.width/2,
        resizeMode:'contain',
        tintColor:'#bdc3c7'
    },
    placeholderText:{
        textAlign:'center',
        fontSize:20,
        color:'#bdc3c7'
    }
}

export default Main;
