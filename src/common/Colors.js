const Colors = {
  primaryBlue: '#2980b9',
  white:'#fff',
  borderColor:'#ccc',
  red:'#c0392b'
}

export default Colors
