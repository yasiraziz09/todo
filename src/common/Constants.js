import { Dimensions, Platform } from 'react-native'
const {
  height: SCREEN_HEIGHT,
  width
} = Dimensions.get('window');
const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;

const Constants = {
  baseurl: "https://www.lhealp.com/api/",
  height:SCREEN_HEIGHT,
  width,
  calendar:require('../assets/calendar.png'),
  check:require('../assets/check.png')
}
export default Constants;