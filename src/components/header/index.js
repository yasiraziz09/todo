import React from 'react';
import { View, Text } from 'react-native';
import Constants from '../../common/Constants';

const Header = ({
    barStyle=null,
    titleText='',
    statusBarHeight=32,
}) => (
    <View>
        <View style={{height:statusBarHeight}} />
        <View style={[{widht:"100%",height:55,paddingVertical: 5, flexDirection:'row',paddingHorizontal: 20,alignItems: 'center',},barStyle]}>
            <Text  style={styles.titleText} numberOfLines={1}>{titleText}</Text>
        </View>
    </View>
);

const styles = {
    subView:{
        marginHorizontal: 10,
        flex:1,
        backgroundColor:"#f7f7f7",
        flexDirection:'row',
        height:45,
        alignItems: 'center',
        paddingHorizontal:15,
        borderRadius: 50,
    },
    titleText:{
        paddingHorizontal:12,
        textAlign:'center',
        flex:1,
        fontFamily: Constants.fontFamily,
        fontSize: 23,
    }
}

export {Header};
