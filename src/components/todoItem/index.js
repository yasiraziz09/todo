import React from 'react';
import { Text, View,Pressable, Image } from 'react-native';
import { Colors, Constants } from '../../common';

const TodoItem = ({
    onPress,
    showDoneButton=true,
    item,
    index
}) => (
    <View style={styles.todoItem}>
        {showDoneButton ? <Pressable onPress={() => onPress(item,index)}>
            <Image style={styles.checkIcon} source={Constants.check} />
        </Pressable> : null}
        <Text style={styles.todoItemTitle}>{item.title}</Text>
        <Text style={styles.todoItemDate}>{item.date}</Text>
    </View>
);

const styles = {
    checkIcon:{
        width:25,
        height:25,
        marginRight:15,
        resizeMode:'contain',
        tintColor:Colors.borderColor
    },
    todoItem:{
        flexDirection:'row',
        marginBottom:5,
        paddingHorizontal:12,
        height:70,
        alignItems: 'center',
        borderBottomWidth:0.5,
        borderBottomColor:Colors.borderColor
    },
    todoItemTitle:{
        flex:1,
        fontSize:18,
        fontWeight:'600',
        paddingRight:20
    },
    todoItemDate:{
        fontSize:16,
    }
}

export default TodoItem;
