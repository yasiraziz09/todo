import React, { Component } from 'react';
import {View } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


import configureStore from './store/configureStore';
const store = configureStore();
import {Provider} from 'react-redux';
import { persistStore } from 'redux-persist'

import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import Splash from './views/Splash';
import Main from './views/Main';
import CompletedTodos from './views/CompletedTodos';
import Tabbar from './components/tabbar';
let persistor = persistStore(store)
let MainStack = createStackNavigator();
let TabStack = createBottomTabNavigator();


const Tabs = () => (
    <TabStack.Navigator 
    tabBarOptions={{
        activeTintColor: '#e91e63',
        showLabel:true,
    }}
    initialRouteName='main'
    tabBar={props => <Tabbar {...props} />}
    >
        <TabStack.Screen options={{tabBarLabel:'Main'}} name="main" component={Main} />
        <TabStack.Screen options={{tabBarLabel:'Completed'}} name="done" component={CompletedTodos} />
    </TabStack.Navigator>
)
const Routes = () => (
    <MainStack.Navigator 
    screenOptions={{
        cardStyle: { backgroundColor: '#fff' },
        
    }}
    headerMode='none' 
    initialRouteName='splash'
    >
        <MainStack.Screen name="splash" component={Splash} />
        <MainStack.Screen name="main" component={Tabs} />
    </MainStack.Navigator>
)
export default class App extends Component {

  render() {
    return (
        <View style={{ flex: 1,}}>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <NavigationContainer>
                        <Routes />
                    </NavigationContainer>
                </PersistGate>
            </Provider>
        </View>
    );
  }

}