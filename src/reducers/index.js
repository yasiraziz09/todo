import { combineReducers } from 'redux';
import MainReducer from './MainReducer';
import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist'

const persistConfig = {
  key: 'root',
  storage:AsyncStorage,
}

const main = persistReducer(persistConfig, MainReducer)
const AppReducer = combineReducers({
  main,
});

export default AppReducer;
