const INITIAL_STATE = {
    todos:[],
    todosCompleted:[]
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'UPDATE_TODOS':
            return{
                ...state,
                todos:action.payload
            }
        case 'UPDATE_TODOS_COMPLETED':
            return{
                ...state,
                todosCompleted:action.payload
            }
        default:
            return state;
    }
};
